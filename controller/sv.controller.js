// renderDSSV
renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((sv) => {
    var contentTr = `
    <tr>
        <td>${sv.id}</td>
        <td>${sv.name}</td>
        <td>${sv.email}</td>
        <td>0</td>
        <td>
            <button onclick=xoaSinhVien('${sv.id}') class="btn btn-danger">Xóa</button>
            <button onclick=showDSSV_ID('${sv.id}') class="btn btn-warning">Sửa</button>
        </td>
    </tr>`;
    contentHTML += contentTr;
  });
  console.log("contentHTML: ", contentHTML);
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
//hàm show sv
function showDSSV(sv) {
  document.getElementById("txtMaSV").value = `${sv.id}`;
  // document.getElementById("txtMaSV").disabled = true;
  document.getElementById("txtTenSV").value = `${sv.name}`;
  document.getElementById("txtEmail").value = `${sv.email}`;
  document.getElementById("txtPass").value = `${sv.password}`;
  document.getElementById("txtDiemToan").value = `${sv.math}`;
  document.getElementById("txtDiemLy").value = `${sv.physics}`;
  document.getElementById("txtDiemHoa").value = `${sv.chemistry}`;
}

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
