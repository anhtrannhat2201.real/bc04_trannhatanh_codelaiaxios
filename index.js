const BASE_URL = "https://62db6ca1d1d97b9e0c4f3355.mockapi.io";
document.getElementById("txtMaSV").disabled = true;
// lấy DSSV
function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();

      console.log(res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
  tatLoading();
}
getDSSV();
//hàm xóa sinh viên
function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
// hàm showDSSV theo id
function showDSSV_ID(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);

      showDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
//hàm thêm sinh viên
function themSV() {
  batLoading();

  //new sv Object lấy từ form
  let newsV = {
    id: document.getElementById("txtMaSV").value,
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    password: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value,
    physics: document.getElementById("txtDiemLy").value,
    chemistry: document.getElementById("txtDiemHoa").value,
  };

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newsV,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
//hàm cập nhật sinh viên
function capnhatSV() {
  // console.log("yes");
  let capNhat = {
    id: document.getElementById("txtMaSV").value,
    name: document.getElementById("txtTenSV").value,
    email: document.getElementById("txtEmail").value,
    password: document.getElementById("txtPass").value,
    math: document.getElementById("txtDiemToan").value,
    physics: document.getElementById("txtDiemLy").value,
    chemistry: document.getElementById("txtDiemHoa").value,
  };
  batLoading();

  axios({
    url: `${BASE_URL}/sv/${capNhat.id}`,
    method: "PUT",
    data: capNhat,
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
  document.getElementById("txtMaSV").disabled = false;
}
//reset input
function reset() {
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";

  document.getElementById("txtMaSV").value = "";
}
